﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.ConstrainedExecution;
using System.Text;
using Xunit;

namespace Test
{
    public class TestXmlFile
    {
        [Fact]
        public void TestShortOneFileBasic()
        {
            DirectoryInfo temp1 = new Compare().CreateTemp();
            Tuple<string, string> strTuple = PrepareTestXmlSort(temp1);
            Assert.NotEqual(File.ReadAllText(strTuple.Item1), File.ReadAllText(strTuple.Item2));
            Compare.CleanTemp(temp1);
        }

        private Tuple<string,string> PrepareTestXmlSort(DirectoryInfo tmp)
        {
            FileInfo xmlFile = new FileInfo(Resource.GetLocalPath() + @"Samples\FileSamples\XmlSamples\sample1\plant_catalog.xml");
            XmlSort xmlSort = (XmlSort)SortFileFactory.GetSortedFile(xmlFile);
            xmlSort.SortAndMove(tmp.FullName + xmlFile.Name);
            return new Tuple<string, string>(xmlFile.FullName, xmlSort.DestPath);
        }
    }
}
