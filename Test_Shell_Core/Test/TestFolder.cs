﻿using System;
using System.IO;
using Xunit;
using Model;

namespace Test
{
    public class TestCompareFolder
    {
        [Fact]
        public void TestFolderAndFile()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\FolderSamples\Version1", Resource.GetLocalPath() + @"Samples\FolderSamples\Version2").ToCompare();
            Assert.True(result, "pass");
        }
    }
}
