﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Test
{
    public class TestGlobal
    {
        [Fact]
        public void TestAll()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\AllTypeSamples\directory3", Resource.GetLocalPath() + @"Samples\AllTypeSamples\directory4").ToCompare();
            Assert.False(result, "pass");
        }
    }
}
