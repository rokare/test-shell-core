﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Model;
using System.IO;

namespace Test
{
    public class TestArchive
    {
        [Fact]
        public void TestOneArchive()
        {
            Tuple<string, string> strTuple = LoadArchiveExample();
            bool result = new Compare(strTuple.Item1, strTuple.Item2).ToCompare();
            Assert.False(result, "pass");
        }

        [Fact]
        public void TestMultipleArchive()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\AllTypeSamples\directory1", Resource.GetLocalPath() + @"Samples\AllTypeSamples\directory2").ToCompare();
            Assert.False(result, "pass");
        }

        public Tuple<string, string> LoadArchiveExample()
        {
            string archivePath1 = Resource.GetLocalPath() + @"Samples\ArchiveSamples\Samples\s2";
            string archivePath2 = Resource.GetLocalPath() + @"Samples\ArchiveSamples\Samples\s1";
            if (!string.IsNullOrEmpty(archivePath1) && !string.IsNullOrEmpty(archivePath2))
            {
                return new Tuple<string, string>(archivePath1, archivePath2);
            } else
            {
                return null;
            }

        }
    }
}
