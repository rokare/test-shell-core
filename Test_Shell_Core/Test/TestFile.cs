using System;
using Xunit;
using System.IO;
using Model;

namespace Test
{
    public class TestFile
    {
        [Fact]
        public void TestTxtFile()
        {
            bool result = CompareFile.Compare(Resource.GetLocalPath() + @"Samples\FileSamples\t1.txt", Resource.GetLocalPath() + @"Samples\FileSamples\t2.txt");
            Assert.True(result, "pass");
        }
    }
}
