﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using System.Management.Automation;
using Shell;
using Model;

namespace Test
{
    public class TestShell
    {
        [Fact]
        public void TestCompareCmdlet()
        {
            var cmdlet = new CompareFolderCmlet()
            {
                FirstFolder = Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\CSV\folder1",
                SecondFolder = Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\CSV\folder2",
                ConfigFile = Resource.GetLocalPath() + @"Samples\ConfigSamples\configFiles\ConfigTestShell.yaml"
            };
            var enumerator = cmdlet.Invoke().GetEnumerator();
            enumerator.MoveNext();
            var results = enumerator.Current;
            Assert.NotEqual("no difference find between the 2 folders", results);
        }
    }
}
