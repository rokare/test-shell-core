﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Test
{
    public class TestConfiguration
    {
        [Fact]
        public void TestCsvFileWithConfig()
        {
            DirectoryInfo temp1 = new Compare().CreateTemp();
            CsvSort sortFile = LoadCsvSortExample(temp1.FullName);
            bool result = CompareFile.Compare(sortFile.FileInfo.FullName, sortFile.DestPath);
            Compare.CleanTemp(temp1);
            Assert.False(result, "pass");
        }

        [Fact]
        public void TestSortCsvFileWithDeedle()
        {
            DirectoryInfo temp1 = new Compare().CreateTemp();
            CsvSort sortFile = LoadCsvSortExample(temp1.FullName, true);
            bool result = CompareFile.Compare(sortFile.FileInfo.FullName, sortFile.DestPath);
            Compare.CleanTemp(temp1);
            Assert.False(result, "pass");
        }

        [Fact]
        public void TestWithMultipleCsv()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\CSV\folder1", Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\CSV\folder2", LoadConfigExample()).ToCompare();
            Assert.True(result, "pass");
        }

        [Fact]
        public void TestWithMultipleXml()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\XML\sample2", Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\XML\sample3", LoadConfigExample()).ToCompare();
            Assert.True(result, "pass");
        }

        [Fact]
        public void TestWithArchive()
        {
            bool result = new Compare(Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\MIX\mix1", Resource.GetLocalPath() + @"Samples\ConfigSamples\multi\MIX\mix2", LoadConfigExample()).ToCompare();
            Assert.True(result, "pass");
        }
        public CsvSort LoadCsvSortExample(string tempFolder, bool deedleOption = false)
        {
            IConfig config = new ConfigurationCsvSort(Resource.GetLocalPath() + @"Samples\ConfigSamples\test\test\", ';', false, null, 1, ConfigurationCsvSort.FieldType.Int);
            FileInfo fileInfo = new FileInfo(Resource.GetLocalPath() + @"Samples\ConfigSamples\test\test\withoutHeader.csv");
            CsvSort sortFile = (CsvSort)SortFileFactory.GetSortedFile(fileInfo, config);
            if (deedleOption)
            {
                sortFile.SortAndMoveDeedle(tempFolder + fileInfo.Name, true);
            }
            else
            {
                sortFile.SortAndMove(tempFolder + fileInfo.Name, true);
            }
            return sortFile;
        }

        //public Configuration LoadConfigExampleCsv()
        //{
        //    List<IConfig> listConfig = new List<IConfig>();
        //    listConfig.Add(new ConfigurationCsvSort(@"test\authority-statistics", ',', true, "Period", ConfigurationCsvSort.FieldType.Float));
        //    listConfig.Add(new ConfigurationCsvSort(@"test\enterprise-survey", ',', true, "value", ConfigurationCsvSort.FieldType.Int));
        //    listConfig.Add(new ConfigurationCsvSort(@"Szfejsefs\fdiuhfsdf", '*', true, "qesdfsqfds", ConfigurationCsvSort.FieldType.Int));
        //    listConfig.Add(new ConfigurationCsvSort(@"8885888\dfgfdgd", '$', true, "75484747475", ConfigurationCsvSort.FieldType.Int));
        //    listConfig.Add(new ConfigurationCsvSort(@"Szfejsefs\dfgddfg", '*', true, "gdfgg", ConfigurationCsvSort.FieldType.Int));
        //    return new Configuration(listConfig);
        //}
        //public Configuration LoadConfigExampleXml()
        //{
        //    List<IConfig> listConfig = new List<IConfig>();
        //    listConfig.Add(new ConfigurationXmlSort(@"test\eppp", "sdfsf", "dzedzd"));
        //    listConfig.Add(new ConfigurationXmlSort(@"dezd\tttt", "OPP", "dddddddd"));
        //    listConfig.Add(new ConfigurationXmlSort(@"orders", "T", "O_TOTALPRICE"));
        //    listConfig.Add(new ConfigurationXmlSort(@"ddddd", "jjjj", "dd"));
        //    listConfig.Add(new ConfigurationXmlSort(@"dfffff", "djhddhdh", "fdzd"));
        //    return new Configuration(listConfig);
        //}

        public Configuration LoadConfigExample()
        {
            List<IConfig> listConfig = new List<IConfig>();
            listConfig.Add(new ConfigurationXmlSort(@"test\eppp", "sdfsf", "dzedzd"));
            listConfig.Add(new ConfigurationXmlSort(@"dezd\tttt", "OPP", "dddddddd"));
            listConfig.Add(new ConfigurationXmlSort(@"orders", "T", "O_TOTALPRICE"));
            listConfig.Add(new ConfigurationXmlSort(@"ddddd", "jjjj", "dd"));
            listConfig.Add(new ConfigurationXmlSort(@"dfffff", "djhddhdh", "fdzd"));
            listConfig.Add(new ConfigurationCsvSort(@"test\authority-statistics", ',', true, "Period", ConfigurationCsvSort.FieldType.Float));
            listConfig.Add(new ConfigurationCsvSort(@"test\enterprise-survey", ',', true, "value", ConfigurationCsvSort.FieldType.Int));
            listConfig.Add(new ConfigurationCsvSort(@"Szfejsefs\fdiuhfsdf", '*', true, "qesdfsqfds", ConfigurationCsvSort.FieldType.Int));
            listConfig.Add(new ConfigurationCsvSort(@"8885888\dfgfdgd", '$', true, "75484747475", ConfigurationCsvSort.FieldType.Int));
            listConfig.Add(new ConfigurationCsvSort(@"Szfejsefs\dfgddfg", '*', true, "gdfgg", ConfigurationCsvSort.FieldType.Int));
            return new Configuration(listConfig);
        }

        [Fact]
        public void TestLoadConfigFile()
        { 
            Configuration config = new Configuration(Resource.GetLocalPath() + @"\Samples\ConfigSamples\configFiles\CleanConfigFile.yaml");
            Assert.NotNull(config.ListConfig);
        }

        [Fact]
        public void TestUnknowConfigTypeException()
        {
            Action action = () => new Configuration(Resource.GetLocalPath() + @"\Samples\ConfigSamples\configFiles\Exception\UnknowConfigTypeException.yaml");
            Assert.Throws<UnknowConfigTypeNotFoundException>(action);
        }
        [Fact]
        public void TestMisformattedFileException()
        {
            Action action = () => new Configuration(Resource.GetLocalPath() + @"\Samples\ConfigSamples\configFiles\Exception\MisformattedFileException.yaml");
            Assert.Throws<MisformattedFileException>(action);
        }
        [Fact]
        public void TestMissingAttributeException()
        {
            Action action = () => new Configuration(Resource.GetLocalPath() + @"\Samples\ConfigSamples\configFiles\Exception\MissingAttributeException.yaml");
            Assert.Throws<MissingAttributeException>(action);
        }

        [Fact]
        public void TestFormatException()
        {
            Action action = () => new Configuration(Resource.GetLocalPath() + @"\Samples\ConfigSamples\configFiles\Exception\FormatException.yaml");
            Assert.Throws<FormatException>(action);
        }

        public void TestWithRandomFile()
        {

        }
    }
}
