﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace Test
{
    public class TestCsvFile
    {
        [Fact]
        public void TestShortOneFileBasic()
        {
            DirectoryInfo temp1 = new Compare().CreateTemp();
            Tuple<string, string> strTuple = PrepareTestCsvSort(temp1);
            Assert.NotEqual(File.ReadAllText(strTuple.Item1), File.ReadAllText(strTuple.Item2));
            Compare.CleanTemp(temp1);
        }

        private Tuple<string, string> PrepareTestCsvSort(DirectoryInfo tmp)
        {
            FileInfo csvFile = new FileInfo(Resource.GetLocalPath() + @"Samples\FileSamples\CsvSamples\sample1\email.csv");
            CsvSort csvSort = (CsvSort)SortFileFactory.GetSortedFile(csvFile);
            csvSort.SortAndMove(tmp.FullName + csvFile.Name);
            return new Tuple<string, string>(csvFile.FullName, csvSort.DestPath);
        }
    }
}
