﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Management.Automation;
using System.Runtime.CompilerServices;
using System.Text;

namespace Shell
{
    [Cmdlet("Compare", "Folder")]
    public class CompareFolderCmlet : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public string FirstFolder { get; set; }

        [Parameter(Mandatory = true)]
        public string SecondFolder { get; set; }

        [Parameter()]
        public string ConfigFile { get; set; }

        protected override void ProcessRecord()
        {
            CheckParameters();
            bool result;
            Compare compare;
            if(ConfigFile != null)
            {
                compare = new Compare(FirstFolder, SecondFolder, new Configuration(ConfigFile));
            } else
            {
                compare = new Compare(FirstFolder, SecondFolder);
            }
            result = compare.ToCompare();
            if(result)
            {
                WriteObject("no difference find between the 2 folders");
            } else
            {
                //write a detail report of the difference for the future
                WriteObject("difference find between the 2 folders");
            }
            //compare.DeleteTempFolder();
        }

        private void CheckParameters()
        {
            if (string.IsNullOrEmpty(FirstFolder) || string.IsNullOrEmpty(SecondFolder))
            {
                throw new Exception("One of the required field is empty");
            }
            if (!Directory.Exists(FirstFolder) && !Directory.Exists(SecondFolder))
            {
                throw new Exception("One of the folder doesn't exist");
            }

            if (!ConfigFile.Equals(null))
            {
                if (!File.Exists(ConfigFile) )
                {
                    throw new Exception("The configuration file doesn't exist");
                } 
                else if(!Path.GetExtension(ConfigFile).Equals(".yaml"))
                {
                    throw new Exception("The exension of the configuration filename must be .yaml");
                }
            }
        }
    }
}
