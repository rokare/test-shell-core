﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Model
{
    public class ConfigCreator
    {
        private IConfigBuilder BuilderConfig { get; set; }

        public void SetBuilderConfig(IConfigBuilder configBuilder)
        {
            BuilderConfig = configBuilder;
        }
        public void ConstructConfigurationCsvSort(IConfigBuilder configBuilder, Dictionary<string,YamlNodeDetail> dico)
        {
            ConfigurationCsvSortBuilder configCsvSortBuilder = (ConfigurationCsvSortBuilder) configBuilder;
            configCsvSortBuilder.ResetBuilder();
            configCsvSortBuilder.SetAttributes(dico);
        }
        public void ConstructConfigurationXmlSort(IConfigBuilder configBuilder, Dictionary<string, YamlNodeDetail> dico)
        {
            ConfigurationXmlSortBuilder configXmlSortBuilder = (ConfigurationXmlSortBuilder)configBuilder;
            configXmlSortBuilder.ResetBuilder();
            configXmlSortBuilder.SetAttributes(dico);
        }
        //add a new builder for each new configuration type 
    }
}
