﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Model
{
    public class ConfigurationXmlSortBuilder : IConfigBuilder
    {
        private ConfigurationXmlSort Configuration { get; set; }

        private YamlNodeDetail CurrentNode { get; set; }


        public ConfigurationXmlSortBuilder()
        {
            ResetBuilder();
        }
        public ConfigurationXmlSortBuilder SetElement(string element)
        {
            Configuration.Element = element;
            return this;
        }
        public ConfigurationXmlSortBuilder SetPath(string path)
        {
            Configuration.Path = path;
            return this;
        }
        public ConfigurationXmlSortBuilder SetAttribute(string attribute)
        {
            Configuration.Attribute = attribute;
            return this;
        }
        public IConfig GetConfig()
        {
            ConfigurationXmlSort config = Configuration;
            ResetBuilder();
            return config;
        }

        public void ResetBuilder()
        {
            Configuration = new ConfigurationXmlSort();
        }

        public void SetAttributes(Dictionary<string, YamlNodeDetail> dico)
        {
            try
            {
                if (!string.IsNullOrEmpty(dico["Element"].Value) && !string.IsNullOrEmpty(dico["Element"].Value))
                {
                    SetAttribute(dico["Attribute"].Value)
                        .SetElement(dico["Element"].Value)
                        .SetPath(dico["Path"].Value);
                }
                else
                {
                    throw new Exception("An attribute "
                        + " is null or empty for the XmlSort type  at approximately line : "
                        + dico["ConfigType"].Line.ToString() + " , at column : "
                        + dico["ConfigType"].Column.ToString());
                }
            }
            catch (KeyNotFoundException keyException)
            {
                string message = keyException.Message;
                Regex regex = new Regex("'(.*?)'");
                MatchCollection matches = regex.Matches(message);
                string missingKey = matches[0].Value.Replace("'", "");
                throw new MissingAttributeException("This attribute : " + missingKey
                    + " is missing for the XmlSort type  at approximately line : "
                    + dico["ConfigType"].Line.ToString() + " , at column : "
                    + dico["ConfigType"].Column.ToString());
            }
        }

        public ConfigurationXmlSortBuilder SetCurrentNode(string key, string type, YamlNodeDetail node)
        {
            CurrentNode = node.AddDetailForException(key, type);
            return this;
        }
    }
}
