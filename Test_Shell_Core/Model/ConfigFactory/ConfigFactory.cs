﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class ConfigFactory
    {
        public static IConfig CreateConfig(string type, Dictionary<string, YamlNodeDetail> dico)
        {
            ConfigType configType = GetConfigType(type);
            IConfigBuilder configBuilder;
            IConfig config;
            ConfigCreator configCreator = new ConfigCreator();
            switch(configType)
            {
                case ConfigType.CsvSort:
                    configBuilder = new ConfigurationCsvSortBuilder();
                    configCreator.ConstructConfigurationCsvSort(configBuilder, dico);
                    config = configBuilder.GetConfig();
                    break;
                case ConfigType.XmlSort:
                    configBuilder = new ConfigurationXmlSortBuilder();
                    configCreator.ConstructConfigurationXmlSort(configBuilder, dico);
                    config = configBuilder.GetConfig();
                    break;
                default:
                    config = null;
                    break;
            }
            return config;
        }
        private static ConfigType GetConfigType(string type)
        {
            try
            {
                return (ConfigType)Enum.Parse(typeof(ConfigType), type);
            } 
            catch (ArgumentException)
            {
                throw new UnknowConfigTypeNotFoundException(type);
            }
        }

    }
}
