﻿using System.Collections.Generic;

namespace Model
{
    public interface IConfigBuilder
    {
        void ResetBuilder();
        IConfig GetConfig();
        void SetAttributes(Dictionary<string, YamlNodeDetail> dico);
    }
}
