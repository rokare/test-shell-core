﻿using Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace Model
{
    public class ConfigurationCsvSortBuilder : IConfigBuilder
    {
        private ConfigurationCsvSort Configuration { get; set; }

        private YamlNodeDetail CurrentNode { get; set; }
        public ConfigurationCsvSortBuilder()
        {
            ResetBuilder();
        }
        public ConfigurationCsvSortBuilder SetColumnName(string name)
        {
            Configuration.ColumnName = name;
            return this;
        }

        public ConfigurationCsvSortBuilder SetIndexColumn(int? index)
        {
            Configuration.IndexColumn = index;
            return this;
        }

        public ConfigurationCsvSortBuilder SetSeparator(char separator)
        {
            Configuration.Separator = separator;
            return this;
        }

        public ConfigurationCsvSortBuilder SetHasHeader(bool hasHeader)
        {
            Configuration.HasHeader = hasHeader;
            return this;
        }

        public ConfigurationCsvSortBuilder SetPath(string path)
        {
            Configuration.Path = path;
            return this;
        }
        public IConfig GetConfig()
        {
            ConfigurationCsvSort config = Configuration;
            ResetBuilder();
            return config;
        }
        public void ResetBuilder()
        {
            Configuration = new ConfigurationCsvSort();
        }
        public ConfigurationCsvSortBuilder SetCurrentNode(string key, string type, YamlNodeDetail node)
        {
            CurrentNode = node.AddDetailForException(key, type);
            return this;
        }
        public void SetAttributes(Dictionary<string, YamlNodeDetail> dico)
        {
            try
            {
                if((string.IsNullOrEmpty(dico["ColumnName"].Value) || dico["ColumnName"].Value.Equals("null")) 
                    && (string.IsNullOrEmpty(dico["ColumnName"].Value) || dico["IndexColumn"].Value.Equals("null")))
                {
                    throw new Exception("Column and IndexColumn fields can't be both null or empty"
                    + " is missing for the CsvSort type  at approximately line : "
                    + dico["ConfigType"].Line.ToString() + " , at column : "
                    + dico["ConfigType"].Column.ToString());
                }
                //need refactor later
                SetCurrentNode("ColumnName", "string", dico["ColumnName"]).SetColumnName(dico["ColumnName"].Value);
                SetCurrentNode("HasHeader", "bool", dico["HasHeader"]).SetHasHeader(Convert.ToBoolean(dico["HasHeader"].Value));
                SetCurrentNode("Path", "string", dico["Path"]).SetPath(dico["Path"].Value);
                SetCurrentNode("Separator", "char", dico["Separator"]).SetSeparator(Convert.ToChar(dico["Separator"].Value));
                SetCurrentNode("IndexColumn", "int", dico["IndexColumn"]).SetIndexColumn(dico["IndexColumn"].Value.Equals("null") ? (int?)null : Convert.ToInt32(dico["IndexColumn"].Value));
            }
            catch (KeyNotFoundException keyException)
            {
                string message = keyException.Message;
                Regex regex = new Regex("'(.*?)'");
                MatchCollection matches = regex.Matches(message);
                string missingKey = matches[0].Value.Replace("'", "");
                throw new MissingAttributeException("This attribute : " + missingKey 
                    + " is missing for the CsvSort type  at approximately line : "
                    + dico["ConfigType"].Line.ToString() + " , at column : "
                    + dico["ConfigType"].Column.ToString());
            }
            catch(FormatException formatException)
            {
                if(CurrentNode.Type != "int")
                {
                    throw new FormatException(formatException.Message 
                        + " At line : " + CurrentNode.Line 
                        + ", at column : " + CurrentNode.Column 
                        + ". \n Check the correct format of the key : "+ CurrentNode.Key
                        + ", for the CsvSort type");
                } 
                else
                {
                    throw new FormatException("The key : " + CurrentNode.Key + " accepts only integer value."
                        + "\nAt line : " + CurrentNode.Line
                        + ", at column : " + CurrentNode.Column
                        + ", for the CsvSort type");
                }
            }
        }
    }
}
