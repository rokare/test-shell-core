﻿namespace Model
{
    public class YamlNodeDetail
    {
        public string Value { get; set; }
        public int Line { get; set; }
    
        public int Column { get; set; }

        public string Key { get; set; }
        public string Type { get; set; }

        public YamlNodeDetail(string value, int line, int column)
        {
            Value = value;
            Line = line;
            Column = column;
        }
        public YamlNodeDetail AddDetailForException(string key, string type)
        {
            Key = key;
            Type = type;
            return this;
        }
    }
}
