﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    class UnknowConfigTypeNotFoundException : SystemException
    {
        public UnknowConfigTypeNotFoundException()
        {
        }

        public UnknowConfigTypeNotFoundException(string type) : base("The type " + type + " not exists")
        {
        }
    }
}
