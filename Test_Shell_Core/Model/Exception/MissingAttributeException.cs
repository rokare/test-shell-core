﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    class MissingAttributeException : System.Exception
    {
        public MissingAttributeException() { }
        public MissingAttributeException(string attribute) : base(attribute)
        {
        }
    }
}
