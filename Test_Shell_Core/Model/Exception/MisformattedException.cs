﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class MisformattedFileException : SystemException 
    {
        public MisformattedFileException() { }
        public MisformattedFileException(string message) : base(message) { }
    }
}
