﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model
{
    public static class Resource
    {
        public static string GetLocalPath()
        {
            return System.IO.Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\"));
        }
    }
}
