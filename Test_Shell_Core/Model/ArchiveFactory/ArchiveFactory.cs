﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model
{
    public class ArchiveFactory
    {
        public static IExtractable GetArchive(FileInfo file)
        {
            switch (GetType(file))
            {
                case Extractable.Zip :
                    return new ZipArchive(file);       
                case Extractable.TarGz:
                    return new TarGzArchive(file);      
                default:
                    return null;
            }
        }
        public static Extractable GetType(FileInfo file)
        {
            string ext = file.Extension;
            if (file.Name.Contains(".tar"))
                ext = ".tar" + ext;
            switch (ext)
            {
                case ".tar.gz":
                    return Extractable.TarGz;
                case ".zip":
                    return Extractable.Zip;
                default:
                    return Extractable.None;
            }

        }
    }
}
