﻿using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;


namespace Model
{
    public class TarGzArchive : IExtractable
    {
        public FileInfo FileInfo;
        public string DestPath;
        public TarGzArchive(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
        }
        public string Extract(string extractFolder)
        {
            DestPath = extractFolder + System.IO.Path.GetFileNameWithoutExtension(FileInfo.FullName);
            DestPath = DestPath.Replace(".tar", "");
            if (!Directory.Exists(DestPath))
            {
                Directory.CreateDirectory(DestPath);
            }
            Stream stream = File.OpenRead(FileInfo.FullName);
            Stream gzipStream = new GZipInputStream(stream);
            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
            tarArchive.ExtractContents(DestPath);
            tarArchive.Close();
            gzipStream.Close();
            stream.Close();
            return DestPath;
        }

        public void Extract(FileInfo fileInfo, string extractFolder)
        {
            DestPath = extractFolder;
            DestPath = DestPath.Replace(".tar", "").Replace(".gz", "");
            if (!Directory.Exists(DestPath))
            {
                Directory.CreateDirectory(DestPath);
            }
            Stream stream = File.OpenRead(fileInfo.FullName);
            Stream gzipStream = new GZipInputStream(stream);
            TarArchive tarArchive = TarArchive.CreateInputTarArchive(gzipStream);
            tarArchive.ExtractContents(DestPath);
            tarArchive.Close();
            gzipStream.Close();
            stream.Close();
        }

        public ConfigType GetConfigType()
        {
            //will be refactor later
            ConfigType configType = ConfigType.None;
            using (MemoryStream memStream = new MemoryStream(File.ReadAllBytes(FileInfo.FullName), false))
            using (GZipInputStream gzStream = new GZipInputStream(memStream))
            using (TarInputStream tarStream = new TarInputStream(gzStream))
            {
                TarEntry entry;
                entry = tarStream.GetNextEntry();
                while (entry != null)
                {
                    if (entry.Size != 0 && !string.IsNullOrEmpty(entry.Name))
                    {
                        if (entry.Name.EndsWith(".xml"))
                        {
                            configType = ConfigType.XmlSort;
                        }
                        else if(entry.Name.EndsWith(".xml"))
                        {
                            configType = ConfigType.CsvSort;
                        }
                        else
                        {
                            configType = ConfigType.None;
                        }
                    }

                    entry = tarStream.GetNextEntry();
                }

            }
            return configType;
        }

        public void SortAndExtract(FileInfo fileInfo, string extractFolder, IConfig config)
        {
            string tmpPathFolder = extractFolder + @"\" + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + @"\";
            DirectoryInfo tmpFolder = Directory.CreateDirectory(tmpPathFolder);
            Extract(fileInfo, tmpFolder.FullName);
            DestPath = extractFolder;
            DestPath = DestPath.Replace(".tar", "").Replace(".gz", "");
            foreach (var file in tmpFolder.GetFiles())
            {
                if(SortFileFactory.GetFileType(file) != FileType.None)
                {
                    ISortFile sortFile = SortFileFactory.GetSortedFile(file, config);
                    sortFile.SortAndMove(DestPath);
                }
                else
                {
                    File.Copy(file.FullName, DestPath);
                }
            }
            tmpFolder.Delete(true);
        }
    }
}
