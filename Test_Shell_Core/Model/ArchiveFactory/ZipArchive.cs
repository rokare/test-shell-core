﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Model
{
    public class ZipArchive : IExtractable
    {
        public FileInfo FileInfo;
        public string DestPath;
        public ZipArchive(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
        }
        public string Extract(string extractFolder)
        {
            DestPath = extractFolder + Path.GetFileNameWithoutExtension(FileInfo.FullName);
            if (!Directory.Exists(DestPath))
            {
                Directory.CreateDirectory(DestPath);
            }
            FastZip fastZip = new FastZip();
            fastZip.ExtractZip(FileInfo.FullName, DestPath, null);
            return DestPath;
        }

        public void Extract(FileInfo fileInfo, string extractFolder)
        {
            DestPath = extractFolder.Replace(".zip", "");
            if (!Directory.Exists(DestPath))
            {
                Directory.CreateDirectory(DestPath);
            }
            FastZip fastZip = new FastZip();
            fastZip.ExtractZip(fileInfo.FullName, DestPath, null);
        }

        public ConfigType GetConfigType()
        {
            //will be refactor later
            ConfigType configType = ConfigType.None;
            using (System.IO.Compression.ZipArchive archive = System.IO.Compression.ZipFile.OpenRead(FileInfo.FullName))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    if (!string.IsNullOrEmpty(entry.Name))
                    {
                        if (entry.Name.EndsWith(".xml"))
                        {
                            configType = ConfigType.XmlSort;
                        }
                        else if (entry.Name.EndsWith(".xml"))
                        {
                            configType = ConfigType.CsvSort;
                        }
                        else
                        {
                            configType = ConfigType.None;
                        }
                    }
                }
            }
            return configType;
        }

        public void SortAndExtract(FileInfo fileInfo, string extractFolder, IConfig config)
        {
            string tmpPathFolder = extractFolder + @"\" + Path.GetFileNameWithoutExtension(Path.GetTempFileName()) + @"\";
            DirectoryInfo tmpFolder = Directory.CreateDirectory(tmpPathFolder);
            Extract(fileInfo, tmpFolder.FullName);
            DestPath = extractFolder;
            foreach (var file in tmpFolder.GetFiles())
            {
                if (SortFileFactory.GetFileType(file) != FileType.None)
                {
                    ISortFile sortFile = SortFileFactory.GetSortedFile(file, config);
                    sortFile.SortAndMove(DestPath);
                }
                else
                {
                    File.Copy(file.FullName, DestPath);
                }
            }
            tmpFolder.Delete(true);
        }
    }
}
