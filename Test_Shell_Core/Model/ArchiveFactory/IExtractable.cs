﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model
{
    [Flags]
    public enum Extractable
    {
        None = 0,
        Zip = 1 << 0,
        TarGz = 1 << 1
    }
    public interface IExtractable
    {
        string Extract(string extractFolder);
        void Extract(FileInfo fileInfo, string extractFolder);
        void SortAndExtract(FileInfo fileInfo, string extractFolder, IConfig config);
        ConfigType GetConfigType();
    }
}
