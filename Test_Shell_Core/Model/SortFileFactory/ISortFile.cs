﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    [Flags]
    public enum FileType
    {
        None = 0,
        Csv = 1 << 0,
        Xml = 1 << 1
    }
    public interface ISortFile
    {
        void SortAndMove(string destPath);
        void SortAndMove(string destPath, bool withConfig);
    }
}
