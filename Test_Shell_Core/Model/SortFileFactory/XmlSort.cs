﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Deedle;

namespace Model
{
    public class XmlSort : ISortFile
    {
        public FileInfo FileInfo { get; set; }
        public string DestPath { get; set; }

        private ConfigurationXmlSort ConfigurationXmlSort;
        public XmlSort(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
        }
        public XmlSort(FileInfo fileInfo, IConfig config)
        {
            FileInfo = fileInfo;
            ConfigurationXmlSort = (ConfigurationXmlSort)config;
        }
        //for test 
        public void SortAndMove(string destPath)
        {
            DestPath = destPath;
            var baseXml = XElement.Load(FileInfo.FullName);

            var orderNodes = baseXml.Elements("PLANT")
                            .OrderBy(x => x.Element("PRICE").Value)
                            .ToList();
            baseXml.RemoveAll();
            orderNodes.ForEach(x => baseXml.Add(x));
            baseXml.Save(DestPath);
        }

        public void SortAndMove(string destPath, bool withConfig)
        {
            DestPath = destPath;
            var baseXml = XElement.Load(FileInfo.FullName);

            var orderNodes = baseXml.Elements(ConfigurationXmlSort.Element)
                            .OrderBy(x => x.Element(ConfigurationXmlSort.Attribute).Value)
                            .ToList();
            baseXml.RemoveAll();
            orderNodes.ForEach(x => baseXml.Add(x));
            baseXml.Save(DestPath);
        }
    }
}
