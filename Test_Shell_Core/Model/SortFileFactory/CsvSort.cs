﻿using Deedle;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace Model
{
    public class CsvSort : ISortFile
    {
        public FileInfo FileInfo { get; set; }
        public string DestPath { get; set; }
        private ConfigurationCsvSort ConfigurationCsvSort;
        public CsvSort(FileInfo fileInfo)
        {
            FileInfo = fileInfo;
        }
        public CsvSort(FileInfo fileInfo, IConfig config)
        {
            FileInfo = fileInfo;
            ConfigurationCsvSort = (ConfigurationCsvSort) config;
        }
        //use only for test
        public void SortAndMove(string destPath)
        {
            var lines = File.ReadLines(FileInfo.FullName, Encoding.Default);
            var data = lines
                       .Skip(1)
                       .Select(l => new { Fields = l.Split(';'), Line = l })
                       .Where(x => x.Fields[1].All(Char.IsDigit))
                       .OrderBy(x => int.Parse(x.Fields[1]))
                       .Select(x => x.Line);
            DestPath = destPath;
            File.WriteAllLines(DestPath, lines.Take(1).Concat(data), Encoding.Default);
        }

        public void SortAndMove(string destPath, bool withConfig)
        {
            if(ConfigurationCsvSort.IndexColumn.Equals(null) && !string.IsNullOrEmpty(ConfigurationCsvSort.ColumnName) && ConfigurationCsvSort.HasHeader)
            {
                ConfigurationCsvSort.IndexColumn = GetIndexFromColumnName(ConfigurationCsvSort.ColumnName);
            }
            int header = (ConfigurationCsvSort.HasHeader) ? 1 : 0;
            var lines = File.ReadLines(FileInfo.FullName, Encoding.Default);
            var data = lines
                       .Skip(header)
                       .Select(l => new { Fields = l.Split(ConfigurationCsvSort.Separator), Line = l })
                        .Where(x => VerifyType(x.Fields[ConfigurationCsvSort.IndexColumn.Value]))
                       .OrderBy(x => x.Fields[ConfigurationCsvSort.IndexColumn.Value])
                       .Select(x => x.Line);
            DestPath = destPath;
            File.WriteAllLines(DestPath, lines.Take(1).Concat(data), Encoding.Default);
        }

        public void SortAndMoveDeedle(string destPath, bool withConfig)
        { 
            var dataFrame = Frame.ReadCsv(FileInfo.FullName, ConfigurationCsvSort.HasHeader, null, null, null, ConfigurationCsvSort.Separator.ToString());
            Frame<int, string> dataFrameSorted;
            DestPath = destPath;
            if (ConfigurationCsvSort.HasHeader)
            {
                dataFrameSorted = dataFrame.SortRows(ConfigurationCsvSort.ColumnName);
                dataFrameSorted.SaveCsv(destPath, false, null, ConfigurationCsvSort.Separator, null);
            } else
            {
                List<string> tempIndex = new List<string>();
                for(int i = 0; i< dataFrame.ColumnCount; i++)
                {
                    tempIndex.Add("col" + i);
                }
                dataFrameSorted = dataFrame
                    .IndexColumnsWith(tempIndex);
                dataFrameSorted = dataFrameSorted.SortRows("col" + ConfigurationCsvSort.IndexColumn);

                dataFrameSorted.SaveCsv(DestPath, false, null, ConfigurationCsvSort.Separator, null);
                File.WriteAllLines(DestPath, File.ReadAllLines(DestPath).Skip(1));
            }
        }
 
        public bool VerifyType (string str)
        {
            switch (ConfigurationCsvSort.Type)
            {
                case ConfigurationCsvSort.FieldType.Int:
                    return str.All(Char.IsDigit);
                default:
                    break;
            }
            return true;
        }
        public int GetIndexFromColumnName(string columnName)
        {
            var lines = File.ReadLines(FileInfo.FullName, Encoding.Default);
            var headersLine = lines.ToArray()[0];
            string[] headers = headersLine.Split(ConfigurationCsvSort.Separator);
            for(int i = 0; i < headers.Length; i++ )
            {
                if (headers[i].Equals(columnName))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
