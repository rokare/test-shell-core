﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Model
{
    public static class SortFileFactory
    {
        public static ISortFile GetSortedFile(FileInfo fileInfo, IConfig config = null)
        {
            FileType fileType = GetFileType(fileInfo);
            switch (fileType)
            {
                case FileType.Csv:
                    if(config != null)
                        return new CsvSort(fileInfo, config);
                    return new CsvSort(fileInfo);
                case FileType.Xml:
                    if (config != null)
                        return new XmlSort(fileInfo, config);
                    return new XmlSort(fileInfo);
                default:
                    return null;
            }
        }

        public static FileType GetFileType(FileInfo fileInfo)
        {
            switch (fileInfo.Extension)
            {
                case ".csv":
                    return FileType.Csv;
                case ".xml":
                    return FileType.Xml;
                default:
                    return FileType.None;
            }
        }
    }

}
