﻿using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Model
{
    public abstract class AbstractConfigurationFileSort : IConfigFile
    {
        public string Path { get; set; }
        public abstract ConfigType GetConfigType();

        public string GetPath()
        {
            return Path;
        }

        public enum FieldType 
        {
            None = 0,
            String = 1 << 0,
            Int = 1 << 1,
            Float = 1 << 2,
            DateTime = 1 << 3
        }

        public bool GetRightConfig(object identity)
        {
            DirectoryInfo dir = new DirectoryInfo((string)identity);
            var directories = Path.Split(@"\").Reverse();
            foreach (var directory in directories)
            {
                dir = dir.Parent;
                if (!dir.Name.Equals(directory))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
