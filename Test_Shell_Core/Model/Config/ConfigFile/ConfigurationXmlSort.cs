﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    public class ConfigurationXmlSort : AbstractConfigurationFileSort
    {

        public string Element { get; set; }
        public string Attribute { get; set; }


        public ConfigurationXmlSort() { }

        public ConfigurationXmlSort(string path, string element, string attribute)
        {
            Path = path;
            Element = element;
            Attribute = attribute;
        }

        public override ConfigType GetConfigType()
        {
            return ConfigType.XmlSort;
        }

    }
}
