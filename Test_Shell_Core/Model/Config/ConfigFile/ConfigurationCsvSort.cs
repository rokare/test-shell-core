﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Model
{
    public class ConfigurationCsvSort : AbstractConfigurationFileSort
    {
        public string ColumnName { get; set; }
        public int? IndexColumn { get; set; }
        public bool HasHeader { get; set; }
        public char Separator { get; set; }
        public FieldType Type { get; set; }

        public ConfigurationCsvSort() { }
        public ConfigurationCsvSort(string path, char separator, bool hasHeader, string columnName, FieldType type)
        {
            Path = path;
            Separator = separator;
            HasHeader = hasHeader;
            ColumnName = columnName;
            Type = type;
        }
        public ConfigurationCsvSort(string path, char separator, bool hasHeader, string columnName, int indexColumn, FieldType type)
        {
            Path = path;
            Separator = separator;
            HasHeader = hasHeader;
            ColumnName = columnName;
            IndexColumn = indexColumn;
            Type = type;
        }
        public override ConfigType GetConfigType()
        {
            return ConfigType.CsvSort;
        }
      
    }
}
