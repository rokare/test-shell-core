﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model
{
    [Flags]
    public enum ConfigType
    {
        None = 0,
        CsvSort = 1 << 0,
        XmlSort = 1 << 1
    }
    public interface IConfig
    {
        ConfigType GetConfigType();
        bool GetRightConfig(object identity);
    }
}
