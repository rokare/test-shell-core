﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YamlDotNet.RepresentationModel;

namespace Model
{
    public class Configuration
    {

        public List<IConfig> ListConfig;
        public Configuration()
        {
            ListConfig = new List<IConfig>();
        }
        public Configuration(List<IConfig> listConfig)
        {
            ListConfig = listConfig;
        }

        public Configuration(string filePath)
        {
            ListConfig = new List<IConfig>();
            LoadConfigFile(filePath);
        }

        public void LoadConfigFile(string filePath)
        {
            string str = File.ReadAllText(filePath);
            YamlStream yaml = new YamlStream();
            yaml.Load(new StringReader(str));
            YamlMappingNode mapping;
            try
            {
                mapping = (YamlMappingNode) yaml.Documents[0].RootNode;
            } catch(InvalidCastException)
            {
                throw new MisformattedFileException();
            }

            YamlSequenceNode configNode = (YamlSequenceNode)mapping.Children[new YamlScalarNode("configs")];
            foreach (YamlMappingNode item in configNode)
            {
                Dictionary<string, YamlNodeDetail> dico = new Dictionary<string, YamlNodeDetail>();
                string type = item.Children["ConfigType"].ToString();
                item.Children.ToList().ForEach( x =>
                {
                    dico.Add(x.Key.ToString(), new YamlNodeDetail(x.Value.ToString(), x.Value.End.Line, x.Value.End.Column));
                });
                ListConfig.Add(ConfigFactory.CreateConfig(type, dico));
            }
        }

        public IConfig GetConfig(ConfigType configtype, object identity)
        {
            foreach(IConfig config in ListConfig)
            {
                if(config.GetRightConfig(identity) && configtype.Equals(config.GetConfigType()))
                {
                    return config;
                }
            }
            return null;
        }

        public static ConfigType GetConfigType(object obj)
        {
            //add a case for each confid added
            switch (obj.GetType().Name)
            {
                case "FileInfo":
                    FileInfo fileInfo = (FileInfo) obj;
                    switch (fileInfo.Extension)
                    {
                        case ".csv":
                            return ConfigType.CsvSort;
                        case ".xml":
                            return ConfigType.XmlSort;
                        case ".zip":
                            return new ZipArchive(fileInfo).GetConfigType();
                        case ".gz":
                            return new TarGzArchive(fileInfo).GetConfigType();
                        default: 
                            return ConfigType.None;
                    }

                default:
                    return ConfigType.None;
            }
        }

    }
}
