﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Model
{
    public  class CompareFolder
    {
        string FirstPath { get; set; }
        string SecondPath { get; set; }

        public CompareFolder(string firstPath, string secondPath)
        {
            FirstPath = firstPath;
            SecondPath = secondPath;
        }
        public bool Compare()
        {
            if(Directory.Exists(FirstPath) && Directory.Exists(SecondPath))
            {
                var contentPath1 = Directory
                    .EnumerateFiles(FirstPath, "*", SearchOption.AllDirectories).OrderBy(x => Path.GetFileName(x));
                var contentPath2 = Directory
                    .EnumerateFiles(SecondPath, "*", SearchOption.AllDirectories).OrderBy(x => Path.GetFileName(x));
                var cleanContent1 = contentPath1.Select(x => x.Replace(FirstPath, "")).ToList();
                var cleanContent2 = contentPath2.Select(x => x.Replace(SecondPath, "")).ToList();

                var diffs = cleanContent1.Except(cleanContent2).Distinct().ToArray();
                
                if (diffs.Length > 0 || cleanContent1.Count != cleanContent2.Count)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        public bool Compare(bool withFile)
        {
            var contentPath1 = Directory
                .EnumerateFiles(FirstPath, "*", SearchOption.AllDirectories).OrderBy(x => Path.GetFileName(x));
            var contentPath2 = Directory
                .EnumerateFiles(SecondPath, "*", SearchOption.AllDirectories).OrderBy(x => Path.GetFileName(x));
            if(contentPath1.Count() == contentPath2.Count())
            {
                var cleanContent1 = contentPath1.Select(x => x.Replace(FirstPath, "")).ToList();
                var cleanContent2 = contentPath2.Select(x => x.Replace(SecondPath, "")).ToList();

                var diffs = cleanContent1.Except(cleanContent2).Distinct().ToArray();

                if (diffs.Length > 0 || cleanContent1.Count != cleanContent2.Count)
                {
                    return false;
                }
                for (int i = 0; i < contentPath1.Count(); i++)
                {
                    if(!CompareFile.Compare(contentPath1.ElementAt(i), contentPath2.ElementAt(i)))
                    {
                        return false;
                    }
                }
                return true;
            } else
            {
                return false;
            }
        }

    }
}
