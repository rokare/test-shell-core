﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit.Abstractions;

namespace Model
{
    public class Compare
    {
        public string LocalPath { get; set; }
        public string TempFolder { get; set; }
        public Configuration Configuration;
        public DirectoryInfo FirstPath { get; set; }
        public DirectoryInfo SecondPath { get; set; }
        public Compare()
        {
            LocalPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\"));
            TempFolder = LocalPath + @"TempFolder";
        }
        public Compare(string firstPath, string secondPath)
        {
            FirstPath = new DirectoryInfo(firstPath);
            SecondPath = new DirectoryInfo(secondPath);
            LocalPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\"));
            TempFolder = LocalPath + @"TempFolder";
        }
        public Compare(string firstPath, string secondPath, Configuration configuration)
        {
            FirstPath = new DirectoryInfo(firstPath);
            SecondPath = new DirectoryInfo(secondPath);
            LocalPath = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "..\\..\\..\\"));
            TempFolder = LocalPath + @"TempFolder";
            Configuration = configuration;
        }
        public bool ToCompare()
        {
            DirectoryInfo temp1 = CreateTemp();
            DirectoryInfo temp2 = CreateTemp();
            CopyToTemp(FirstPath, temp1);
            CopyToTemp(SecondPath, temp2);
            CompareFolder compareFolder = new CompareFolder(temp1.FullName, temp2.FullName);
            bool result = compareFolder.Compare(true);
            //CleanTemp(temp1, temp2);
            return result;
        }

        public void CopyToTemp(DirectoryInfo path, DirectoryInfo tempPath)
        {
            var fileInfos = path.GetFiles("*", SearchOption.AllDirectories).OrderBy(x => x.FullName);
            foreach (var item in fileInfos)
            {
                string newPath = GetNewPath(path, item, tempPath);
                if (ArchiveFactory.GetType(item) != Extractable.None)
                    ExtractFile(item, newPath);
                else
                    CopyFile(item, newPath);
            }
        }

        private void CopyFile(FileInfo filePath, string newPath)
        {
            if (Configuration != null)
            {
                CopyFileWithConfig(filePath, newPath);
            }
            else
            {
                File.Copy(filePath.FullName, newPath);
            }
        }
    
        private void CopyFileWithConfig(FileInfo file, string newPath)
        {
            ConfigType type = Configuration.GetConfigType(file);
            IConfig config = Configuration.GetConfig(type, file.FullName);
            if (config != null)
            {
                SortFileFactory.GetSortedFile(file, config).SortAndMove(newPath, true);
            }
            else
            {
                File.Copy(file.FullName, newPath);
            }
        }

        private void ExtractFile(FileInfo filePath, string newPath)
        {
            
            if (Configuration != null )
            {
                ExtractFileWithConfig(filePath, newPath);
            }
            else
            {
                ArchiveFactory.GetArchive(filePath).Extract(filePath, newPath);
            }
        }

        private string GetNewPath(DirectoryInfo path, FileInfo filePath, DirectoryInfo tempPath)
        {
            DirectoryInfo directory = filePath.Directory;
            List<DirectoryInfo> listParentDirectories = new List<DirectoryInfo>();

            while (!directory.FullName.Equals(path.FullName))
            {
                listParentDirectories.Add(directory);
                directory = directory.Parent;
            }

            listParentDirectories.Reverse();
            string tempFilePath = tempPath.FullName;
            foreach (DirectoryInfo dir in listParentDirectories)
            {
                if (dir.Equals(path))
                {
                    break;
                }
                tempFilePath += dir.Name + @"\";
                if (!Directory.Exists(tempFilePath))
                {
                    Directory.CreateDirectory(tempFilePath);
                }
            }
            return tempFilePath += filePath.Name;
        }
        private void ExtractFileWithConfig(FileInfo file, string newPath)
        {
            ConfigType type = Configuration.GetConfigType(file);
            IConfig config = Configuration.GetConfig(type, file.FullName);
            if (config != null)
            {
                ArchiveFactory.GetArchive(file).SortAndExtract(file, newPath, config);
            } else
            {
                ArchiveFactory.GetArchive(file).Extract(file, newPath);
            }
        }
        public DirectoryInfo CreateTemp()
        {
            if(!Directory.Exists(TempFolder))
            {
                Directory.CreateDirectory(TempFolder);
            }
            string tempName = Path.GetFileNameWithoutExtension(Path.GetTempFileName());
            string path = TempFolder+ @"\" + tempName + @"\";
            DirectoryInfo directory = Directory.CreateDirectory(path);
            return directory;
        }

        public void CleanTemp(DirectoryInfo tmp1, DirectoryInfo tmp2)
        {
            tmp1.Delete(true);
            tmp2.Delete(true);
        }
        public static void CleanTemp(DirectoryInfo tempDirectory)
        {
            tempDirectory.Delete(true);
        }
        //only for cmdlet
        public void DeleteTempFolder()
        {
            Directory.Delete(TempFolder, true);
        }
    }
}
