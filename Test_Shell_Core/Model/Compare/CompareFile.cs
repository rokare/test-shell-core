﻿using System;
using System.IO;

namespace Model
{
    public static class CompareFile
    {
        public static bool Compare(string path1, string path2)
        {
            return Equals(File.ReadAllText(path1), File.ReadAllText(path2));
        }
    }
}
